# syncSharedocsNakala

## Observations

Les projets de recherches en SHS utilisent généralement le service sharedocs d'Huma-Num pour le travail collaboratif de création de corpus et sa documentation.

Les chercheurs et documentalistes renomment en appliquant un système de cote les fichiers de leur corpus et les déposent dans les dossiers d'un espace sharedocs dédié au projet. Chaque fichier est richement décrit dans des tableurs.

Il est ensuite question de poser sur une plateforme FAIR le corpus constitué pour le projet. 
Quand il n'y a pas d'entrepôt spécifique/disciplinaire adapté au projet, c'est souvent Nakala d'Huma-Num qui est choisi pour son caractère généraliste et riche en fonctionnalités.

Dans ce cas il faut **modéliser** la structure de données et de métadonnées Nakala la plus adaptés aux fichiers et descriptions du corpus réalisée. 
Une fois la logique de transformation/appariement choisie il faut garantir un dépôt/mise à jour du corpus. 
A cette occasion, apparaît le besoin d'un outil permettant de synchroniser les dossiers/fichiers d'un espace projet sharedocs avec une collection de datas Nakala.

## Objectifs

Proposer une méthode et les codes permettant la synchronisation entre un **corpus Sharedocs** et un **corpus Nakala**.

## Informations sur la communication avec Sharedocs
Le service Sharedocs peut être accessible en lecture/écriture depuis le protocole WebDav.
Il est tout à fait possible de communiquer sur ce protocole avec une librairie python dédiée tels que [webdavclient3](https://pypi.org/project/webdavclient3/).

Mais il est également tout à fait possible de se passer d'utiliser une telle librairie en montant son espace sharedocs tels un disque réseau sur son système.
Une fois cela fait, la lecture des fichiers du sharedocs se fait comme si les fichiers distants étaient accessibles en local sur sa machine.



### Montage de son espace sharedocs en disque réseaux (Windows)

Depuis l'explorateur de fichier. Faire clic Droit sur **Réseaux**, puis choisir **Connecter un lecteur réseau**.

<img src="./images/connecterLecteurRes.jpg" alt="Connecter un lecteur reseau" width="400"/>


Saisir dans le champ **Dossier** l'url : https://sharedocs.huma-num.fr/dav.php/ et sélectionner **se connecter à l'aide d'informations d'identification différentes**

<img src="./images/configUrl.jpg" alt="Configuration url sharedocs" width="400"/>

Saisir ses **informations d'identifications**.

<img src="./images/configLogPass.jpg" alt="Configuration url sharedocs" width="400"/>

Pour obtenir ses **informations d'identifications** il faut aller sur son interface web sharedocs. Puis choisir **Connect App** sur son compte utilisateur.

<img src="./images/compteSharedocs.jpg" alt="Configuration url sharedocs" width="400"/>

Après avoir choisi **Acceptez** vous pouvez obtenir vos informations d'identifications.

<img src="./images/idswebdav.jpg" alt="id webdav" width="400"/>


Il est ensuite possible, depuis son explorateur de fichier de naviguer dans les dossiers distants du projet.

<img src="./images/sousdossiers.jpg" alt="navigation dans les dossiers distants" width="400"/>

> Remarque : Windows limite la taille maximale des fichiers téléchargeables par ce protocole à 47Mo. Il est possible d'[augmenter](https://www.strato.fr/faq/stockage-en-ligne/voici-comment-augmenter-la-taille-maximale-de-fichiers-pour-les-telechargements-en-cas-dutilisation-de-webdav-sous-windows/) cette limitation.



## Informations sur la communication avec Nakala

Pour faciliter la communication/synchronisation avec Nakala, et plus précisément son API, depuis du code python, nous utilisons la librairie [nakalaPycon](https://gitlab.huma-num.fr/mshs-poitiers/plateforme/nakalapycon). 

Pour communiquer(créer/modifier) avec l'API Nakala, il faut connaître son identifiant et sa clé d'API. Pour cela, il faut se connecter sur l'interface web de Nakala, cliquer en haut à droite et choisir **Mon profil**.


<img src="./images/apiKeyNakala.jpg" alt="navigation dans les dossiers distants" width="400"/>



## Modélisation de corpus en NklCollection et NklData Nakala

Dans l'entrepôt de données Nkl (Nakala), il y a 4 grands objets à appréhender pour envisager la *modélisation* de son corpus :
1. NklData : un objet numérique contenant un (ou plusieurs fichiers) multimédias + 5 métadonnées descriptives (ou plus)
1. NklCollection : une agrégation de 0 à *N* Data + 1 métadonnées descriptives (ou plus)
1. Nkl_User : un utilisateur Nakala 
1. Nkl_Group : une agrégation de 1 à *N* User

### Bonne pratique sous Nakala
Il est recommandé d'envisager l'administration (création, modification, "suppression") de Data et Collection Nakala d'un projet à plusieurs. Bien qu'en pratique, généralement, une seule personne a la charge du versement des lots de NklData sur Nakala, il faut anticiper "l'indisponibilité" de cette personne (mutation, fin de CDD, vacation, stage...).

Pour partager à plusieurs les droits *administrateur* d'une Data et d'une Collection nous recommandons :

1. Créer un Nkl_Group pour le projet et y ajouter plusieurs Nkl_Users du projet.
1. Créer au moins une NklCollection pour le projet et y ajouter progressivement toutes les NklData du projet (cela permet de retrouver très facilement toutes les Data du projet et permet son moissonnage OAI_PMH par Isidore). 
1. Affecter à chaque NklData du projet des droits d'administration au Nkl_Group.

> Remarque : Les Datas d'une Collection n'héritent pas des droits de la Collection qui les agrèges. Cela est moins surprenant quand on comprend que l'on peut créer sa propre Collection qui ne fait qu'agréger des Data d'autres Nkl_User.

> Remarque : Nakala limite le nombre de Data avec le status "privé" d'un utilisateur (cf. [documentation](https://documentation.huma-num.fr/nakala/#deposer-des-donnees) section **Distinction données déposées / données publiées**) .


### Exemple d'un cas d'usage réaliste

Nous disposons d'un espace projet sur le sharedocs pour le travail collaboratif autours d'un corpus de fichiers audios provenant d'*Enquêtes* de terrains réalisées dans le cadre de différentes *Missions*.

Il y a dans l'espace projet sur le sharedocs autant de dossier que de *Missions*. 
Dans le dossier de chaque *Mission*, il y a autant de dossiers que d'*Enquêtes*. Une enquête étant généralement associé à un *Informateur*.

Dans le dossier de chaque *Enquête*, il y a plusieurs fichiers audios car l'enquêteur a fractionné sa captation sur le terrain ou à son retour. 
Généralement, le fractionnement en plusieurs fichiers audios est lié au sujet qui est traité par l'informateur. 
Par exemple, un informateur commence par se présenter, puis il chante trois chansons et fini par raconter un souvenir. Dans ce cas, le fractionnement a été réalisé en cinq. 
- fichier audio 1 : présentation de l'informateur
- fichier audio 2 : chanson A de l'informateur
- fichier audio 3 : chanson B de l'informateur
- fichier audio 4 : chanson C de l'informateur
- fichier audio 5 : souvenir de l'informateur.

A la racine du dossier *Mission* est travaillé un tableur contenant de nombreuses informations précises, pointues, disciplinaires décrivant chaque enquête et chaque fichier audio.

Il faut maintenant définir une **modélisation** pour cette structuration de données du Sharedocs dans les objets NklData et NklCollection de Nakala.

Dans le cas des données décrites précédemment, nous proposons les modélisations suivantes :

#### Modélisation A

- A chaque *Enquête* correspondra une NklData. Chacune de ces NklData contiendra les *N* fichiers audios "fractionnées".
- A chaque *Mission* correspondra une NklCollection. Chacune de ces NklCollection agrégera les *N* NklData d'*Enquête*.

- Un NklCollection du *Projet* unique agrégera toutes les NklData de toutes les *Mission*.

#### Modélisation B
- A chaque fichier audio correspondra une NklData.
- A chaque *Enquête* correspondra une NklCollection. Chacune de ces NklCollection agrégera les *N* NklData de fichier de l'*Enquête*
- A chaque *Mission* correspondra une NklCollection. Chacune de ces NklCollection agrégera les *N* NklData de fichier de l'*Enquête*.
- Un NklCollection du *Projet* unique agrégera toutes les NklData de toutes les *Mission*.


#### Discussion autour des avantages/inconvénients des modélisations
La modélisation A, exploitant la capacité "multi-file" des NklData, est facile à comprendre car elle est très proche de la structure de dossier/classement du corpus sur le Sharedocs. Ce type de NklData, facilite grandement la navigation entre les différents fichiers audios enregistrés lors d'une même enquête de terrain. Le principal inconvénient porte sur les métadonnées. En effet, sur Nakala, les métadonnées descriptives sont au niveau de la NklData mais pas au niveau de chaque File. Au niveau de chaque File on peut simplement ajouter une métadonnée unique : Description. Dans notre cas, la NklData représente une *Enquête* donc les métadonnées doivent décrire ce niveau d'objet et ne peuvent pas réellement supporter la richesse de description disponibles dans nos tableurs au niveau de chaque Fichier. 

La modélisation B, où une NklData contient uniquement un File permet d'accueillir bien plus facilement la richesse des descriptions disponibles dans nos tableurs au niveau de chaque Fichier. Mais le lien très étroit existant entre chaque Fichier est moins intelligible. Avec le modèle proposé, le lien existe par les NklCollection du niveau *Enquête*. On peut également compléter ce lien avec des métadonnées **dctems:relation**, ce qui peut être pertinent pour des machines, si elles sont prête à valoriser cette connexion.

Bien qu'aucune des modélisations imaginées ne soit parfaite, nous devons en choisir une.

Dans notre cas, nous avons privilégié l'intelligibilité humaine en adoptant la modélisation A. Pour compenser le manque de métadonnées du niveau Fichier, nous proposons de déposer sur Nakala le tableur de métadonnée des documentalistes que nous pouvons compléter avec sa version transformée dans un format XML EAD potentiellement exploitable pour une intégration SUDOC. De plus, en complément de l'hébergement du corpus sur Nakala, nous disposons d'une plateforme web avec une base de données riche et spécifique au corpus pour faciliter l'exploration des données.

En résumé, le corpus est "pérennisé" sur Nakala avec un système minimal de métadonnées, complété par une base de données et site web dédié complémentaire, affranchit des problèmes de stockages gérés par Nakala.

### Formaliser l'appariement de modèles/transformation

Dans l'espace Sharedocs, le corpus est structuré de manière arborescente avec plusieurs niveaux de dossiers, sous-dossiers et nommages structuré, le tout complété par des tableurs décrivant précisément les *Missions*, *Enquêtes* et Fichiers audios.

Dans Nakala, nous avons imaginé une modélisation A compatible avec les objets numériques de l'entrepôt Nakala.

Notre idée consiste à créer un *code de transformation* capable de convertir les tableurs décrivant les *Missions*, *Enquêtes* et Fichiers audios dans un tableur structuré plus facilement compatible avec les objets Nakala et la librairie [nakalaPycon](https://gitlab.huma-num.fr/mshs-poitiers/plateforme/nakalapycon).

Nous souhaitons également alléger le plus possible le *script de transformation* en utilisant un tableur d'appariement/mapping plus facile à modifier/maintenir pour des non-programmeurs (pour faciliter les éventuels changements des tableurs sources et des possibilités de Nakala).

C'est à partir du tableur de données transformées qu'un *code de dépôt/MAJ Nakala* sera assuré la synchronisation Sharedocs/Nakala.  



### Synchronisation

#### Algorithme : syncLocalDir2Nkl

**Objectifs** :
Garantir la synchronisation entre un corpus de fichiers "locaux" et un corpus de NklData/NklCollection sur l'entrepôt Nakala

**Entrées** :
- corpusPjtDir : Un dossier structuré contenant les fichiers et métadonnées descriptives d'un corpus à maintenir à jour sur Nakala. Ce dossier "local" peut-être un dossier "distant" s'il y a eu en amont un montage de disque réseau (cf. section **Montage de son espace sharedocs en disque réseaux**).

- nklTarget : les informations de connexion à Nakala. A savoir, un entrepôt Nakala (Production) ou Nakala (Bac à sable) + une API Key Nakala ayant des droits sur l'entrepôt Nakala ciblé.

- tabMapping : un tableur d'appariement/mapping permettant de configurer les métadonnées Source décrivant le corpus vers des champs de métadonnées Cible de NklData. Element indispensable à la conversion de Modèle.


**Sortie** :
- log : un fichier contenant un bilan de la synchronisation réalisée.

**Grandes Etapes** :

1. modelConvert : Générer un tableur tableurNkl par la conversion du tableurMétier décrivant *Enquete* et *Fichiers* vers un tableur décrivant *NklData* et *Nkl_Files* en utilisant un tableur d'appariement tabMapping (permettant de configurer colonnes *Source* vers colonnes *Cible*).

1. pushPutCorpus : Utiliser le tableurNkl pour déposer et mettre à jour un ensemble de NklData (sous status "Pending").

1. checkNklData : Vérifier semi-automatiquement l'ensemble des NklData créées/MAJ.

1. publishNklData : Si ce n'est pas déjà fait, rendre publique les NklData (sous status "Publish") et les faire aggréger par la NklCollection (public) représentant la Mission.

1. getSyncLog : Générer un rapport permettant de connaître "l'état de la synchronisation" réalisée. 


#### Algorithme : modelConvert

Action pouvant être réalisé par programmation (python, R), ou semi-automatisé (Open refine) ou complétement manuellement (Calc/Excel).
Cf exemple [convertModel/modelConvert.ipynb](https://gitlab.huma-num.fr/mshs-poitiers/plateforme/syncsharedocsnakala/-/blob/main/convertModel/modelConvert.ipynb) utilisant Pandas.

Cette étape de conversion de modèle est très dépendante des tableurs de métadonnées utilisés par les chercheurs/documentalistes en charge de la description du corpus et le modèle de NklData attendu sur Nakala.

Pour cette étape il est important d'arrêter une structure de tableurNkl permettant la création des NklData avec métadonnées et fichiers.

Nous proposons en exemple de [tableurNkl](pushPutCorpus/metasEnqueteItem_NklDataFiles.xlsx), un tableur structuré "minimal" mais extensible permettant la création de plusieurs NklData contenant chacune plusieurs fichiers dans l'entrepôt Nakala.


#### Détails du tableurNkl 

Ce tableur doit contenir de manière structurée toutes les informations permettant l'envoi d'un corpus dans des NklData.
Nous proposons qu'il contienne deux "onglets" ("feuilles"/"tables").

Un onglet dédié aux NklData et un onglet dédié aux NklFile et nous veillerons à créer un lien (type clé primaire/étrangère) pour que les NklData puissent agréger les bons NklFiles.

#### Table NklData

**liste des colonnes :**
- title : le titre de la NklData (titre d'une Enquête dans notre cas)
- creator : une liste de créateur de la nklData (les enquêteur/collecteurs dans notre cas)
- created : la date de création de la nklData (la date de la collecte de l'enquête dans notre cas)
- type : le type de la NklData (type Sound dans notre cas)
- licence : la license de partage/réutilisation (Etalab 2.0 dans notre cas)
- nklIdentifier : le DOI si la NklData a déjà été créée et publié, vide sinon (ou si envie de requêtage dynamique en utilisant un autre identifiant unique). (Vide dans notre cas, car nous allons utiliser un shelfnumber)
- nklStatus : pending ou published
- shelfnumber : une cote archivistique qui alimentera un champ dcterms identifier dans nakala. Dans notre cas nous utiliserons cet identifiant unique en guise de clef primaire et ce même numéro sera utilisé en clé étrangère pour l'agrégation des NklFiles.

#### Table NklFile

**liste des colonnes :**
- data_identifier : sorte de clé étrangère pour savoir à quelle NklData devra être associé de ce fichier
- file : le nom du fichier à déposer
- description : une description facultative en texte libre pour décrire ce fichier dans la NklData
- path : le chemin du dossier "local" où il est possible de trouver le nom du fichier déclaré
- sha1 : l'empreinte numérique du fichier
- embargoed : eventuellement une date de levée d'embargo
- order : un nombre entier permettant le trie croissant de plusieurs NklFile devant appartenir à une même NklData

#### Algorithme : pushPutCorpus

A cette étape, nous partons du principe que nous disposons d'un [tableurNkl](./pushPutCorpus/metasEnqueteItem_NklDataFiles.xlsx) correspondant à nos attentes en terme de création/mise à jour de NklData (contenant un ou plusieurs fichiers).

Chaque NklData est décrite par des metadonnées.

**Attention** sous Nakala, pour préciser dans un champ de métadonnée *Subject DublinCore* la valeur *traditionnal song*, il faut en réalité préciser un quadruplet constitué des éléments suivants :

- **propertyUri**: le champ de métadonnée cible choisi
    - Exemple :  **http://purl.org/dc/terms/subject** pour une métadonnée sujet du DCterms
    
- **value**: la valeur de la métadonnée
    - Exemple :  **traditional song** 
- **lang**: la langue de la value
    - Exemple :  **En** pour préciser English

- **typeUri**: le type de la value
    - Exemple : **http://www.w3.org/2001/XMLSchema#string** pour préciser que notre valeur est une chaine de caractère, ce qui est très souvent le cas. Il peut arriver d'utiliser **http://www.w3.org/2001/XMLSchema#anyURI** quand la valeur est l'url vers un concept référencé dans un thésaurus en ligne comme cela peut être le cas pour les http://nakala.fr/terms#type.
    
Pour préciser ce quadruplet sans alourdir le tableurNkl, nous avons proposé de fixer ces valeurs dans un fichier de configuration additionnel que nous nommons **nklConfColLangTypeProperUri**.

Dans l'algorithme suivant, nous avons pris le parti d'optimiser le concept de mise à jour. Plus précisément, dans le cas d'une Data multi-files, nous commençons par envoyer le premier File, de créer la Data avec ses métadonnées avec ce premier File, puis d'envoyer le File suivant, puis de mettre à jour la Data avec ce File additionnel, puis d'envoyer le File suivant, puis de mettre à jour la Data avec ce File additionnel et ainsi de suite jusqu'a épuisement des Files. L'idée est de mettre à jour très fréquemment la Data après chaque envoi réussi. Ce principe peut être contre intuitif avec le principe plus simple consistant à commencer par envoyer en chaîne tous les Files, d'attendre que tous les Files soient correctement arrivés puis ensuite finir par créer la Data avec ses multi-File.

**Etapes**
```python

Pour chaque Data (ligne) de dfDatas
    on extrait la cote (ou autre colonne précisée comme étant clé primaire)
    on verifie l existance d une data avec cette cote par une requete



    Si il n existe pas cette Data 
        il faut la créer mais avant
        il faut pousser les Files qui lui sont associés (au moins un pour nous)

        isoler les lignes de dfFiles correspondant à la Data actuellement traité
        en utilisant la colonne data_identifier
        trié par order

        Pour chaque File on vérifie si il n est pas déjà dans l espace temporaire nakala uploads

            Si pas dans cet espace upload 
                on l envoi
                Si envoi du fichier réussi 
                    on a son sha1
                    on peut creer un objet fileInfos
                    objet fileInfos : un dico de clés : sha1 , description, embargoed



                Sinon
                    echec de l envoi de ce fichier
                    on passe au fichier suivant

            Sinon
                déjà dans l espace upload

                    on récupère son sha1
                    on peut creer un objet fileInfos
                    objet fileInfos : un dico de clés : sha1 , description, embargoed
                    
            on peut pousser notre Data avec son premier Fichier via l objet fileInfos
            envoyer la Data
            sortir pour revenir à 
                on verifie l existance d une data avec cette cote par une requete

    Sinon
        la Data existe déjà sur Nakala (cas après la création d une Data contenant au moins File)
        donc on passe une section
        d une data existante avec poursuite d envoi des fichiers manquants potentiellement

        on peut obtenir son DOI, et la liste des Files qu elle aggrege déjà
        isoler les lignes de dfFiles correspondant à la Data actuellement traité
        en utilisant la colonne data_identifier   
        trié par order

        Pour chaque File
            vérifier sa présence dans la Data distante
            Si ce File n est pas présent 
                on vérifie si il n est pas déjà dans l espace temporaire nakala uploads
                Si pas présent non plus dans l espace temporaire upload
                    on l envoi
                    Si envoi du fichier réussi on a son sha1
                        on peut creer un objet fileInfos
                        objet fileInfos : un dico de clés : sha1 , description, embargoed
                        
                        on peut pousser une MAJ sur notre Data 
                        avec son Fichier additionnel via l objet fileInfos

                        
                    Sinon
                    echec de l envoi de ce fichier
                        on passe au fichier suivant
                        

```

L'implémentation de cet algorithme est disponible dans le fichier [pushPutCorpus/nklPushPutDatasFiles.py](https://gitlab.huma-num.fr/mshs-poitiers/plateforme/syncsharedocsnakala/-/blob/main/pushPutCorpus/nklPushPutDatasFiles.py).
Il est laissé à la liberté de chacun de s'appropier ce code et de le modifier pour l'adpater à ses besoins si par exemple la structuration des tableursNkl n'est pas appropriée.

Une démonstration de l'utilisation de ce code est disponible dans le fichier [pushPutCorpus/test_nklPushPutDatasFiles.py](https://gitlab.huma-num.fr/mshs-poitiers/plateforme/syncsharedocsnakala/-/blob/main/pushPutCorpus/test_nklPushPutDatasFiles.py)
et de manière plus digeste pour des programmeurs moins expérimentés dans le fichier [pushPutCorpus/demo-PushPutCorpus.ipynb](https://gitlab.huma-num.fr/mshs-poitiers/plateforme/syncsharedocsnakala/-/blob/main/pushPutCorpus/demo-PushPutCorpus.ipynb)

#### Algorithme : checkNklData
....

Regarder les codes [nklBot : Vérification](https://gitlab.huma-num.fr/pbrchd/nkl-bot) Pierre Brochard 

#### Algorithme : publishNklData
....

#### Algorithme : getSyncLog
....
Regarder les codes [nakala-report](https://gitlab.huma-num.fr/mnauge/nakala-report)




























