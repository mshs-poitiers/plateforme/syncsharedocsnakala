# -*- coding: utf-8 -*-
"""
Created on Mon Aug 29 17:04:13 2022

@author: Michael Nauge (Université de Poitiers)

"""

import pandas as pd
import nklPushPutDatasFiles as nkpp

import nakalapycon as nklco


def pushPutDataFiles():
    
    #pathMetas = "./metasEnqueteItem_NklDataFilesMini.xlsx"
    #pathMetas = "./../test/pushPutCorpus/metasEnqueteItem_NklDataFiles.xlsx"
    #pathMetas = "./metasEnqueteItem_NklDataFilesMiniGroupRight.xlsx"
    pathMetas = "./metasEnqueteItem_NklDataFilesMiniGroupRightCollections.xlsx"



    dfD = pd.read_excel(pathMetas, sheet_name='nklDatas', converters={'created':str}, engine="openpyxl")
    #dfD = pd.read_excel(pathMetas, sheet_name='nklDatas', engine="openpyxl")
    # imortant de forcer ce formatage car sinon
    # j'ai un objet date reconnu est donc il rajoute  00:00:00 à la fin de la date lors de la conversion en string plus tard dans le traitement
    #dfD['created'] = dfD['created'].dt.strftime('%Y-%m-%d')

    pathConfCol = "./nklConfColLangTypeProperUri.ods"
    dfC = pd.read_excel(pathConfCol, converters={'Nkl-lang':str}, engine="odf")
    #print(dfC)
    
    pathDfFiles = pathMetas
    dfF = pd.read_excel(pathDfFiles,sheet_name='nklFiles',converters={'sha1':str,'description':str, 'order':str, 'embargoed':str}, engine="openpyxl" )
    #print(dfF)
    
    # unakala3
    apiK = "aae99aba-476e-4ff2-2886-0aaf1bfa6fd2"
    isNklProd = False
    nklCurrentTarget = nklco.NklTarget(isNakalaProd=isNklProd, apiKey=apiK)

    nkpp.pushPutDataFiles(nklCurrentTarget, dfD, dfF, dfC)



def rightsValuesToDic_test():
    # 1 userGroup
    #nkpp.rigthsValuesToDic("f43b285b-69a5-11ed-bd83-52540025b96e,ROLE_ADMIN")
    
    # 2 userGroup
    listResult = nkpp.rightsValuesToDic("f43b285b-69a5-11ed-bd83-52540025b96e,ROLE_ADMIN | 434a81e9-69a6-11ed-9170-5254008c9c26,ROLE_READER")
    
    # bad formatting
    #listResult = nkpp.rigthsValuesToDic("ROLE_READER")

    print(listResult)
    
    
    
def collectionsValuesToDic_test():
    
    #1 collectionId
    #listResult = nkpp.collectionsValuesToDic("10.34847/nkl.2f746415", "|")
    
    #2 collectionId
    listResult = nkpp.collectionsValuesToDic("10.34847/nkl.2f746415 | 10.34847/nkl.2f746415", "|")
    
    # bad formating
    #listResult = nkpp.collectionsValuesToDic("francoraliteTest", "|")
    
    
    
    print(listResult)

    

pushPutDataFiles()
#rigthsValuesToDic_test()
#collectionsValuesToDic_test()