# -*- coding: utf-8 -*-
"""
Created on Mon Aug 29 17:04:13 2022

@author: Michael Nauge (Université de Poitiers)

"""

import pandas as pd

import nakalapycon as nklco


#import nklDf2Dic as nkD2D
import re


def pushPutDataFiles(nklTarget, dfDatas, dfFiles, dfConfCol):
    """
    Envoyer et mettre à jour toutes les datas et leurs fichiers 
    décrit dans les matrices dfDatas, dfFiles
        
    Parameters
    dfDatas : Dataframe
    	une instance d'un objet dataframe pandas contenant des colonnes spécifiques DATA
        
    dfFiles : Dataframe
    	une instance d'un objet dataframe pandas contenant des colonnes spécifiques FILES
        
    dfConfCol : Dataframe
    	une instance d'un objet dataframe pandas contenant des informations permettant
        de préciser la lang, le typeUri, le propertyUri de chaque colonne de dfData
    
        
    Returns
    -------
    NklResponse :
        pass

        
    """    

        
    
    
    # pour chaque Data (ligne) de dfDatas
    # for idx, rowD in dfDatas.iterrows():
    
    # on ne fait pas un parcours classique car on veut pouvoir passer plusieurs fois sur
    # la même ligne car nous réalisons l'envoi complet d'une DATA
    # en plusieurs fois (1 creation avec un File puis autant d'update eque de nombre de File -1)
        
    lenDfD = len(dfDatas)
    i = 0
    
    dataNklIdChecked = ""
    needDataMaj = False

    # pour chaque Data (ligne) de dfDatas
    while i < lenDfD:
        
        print(">>>>>>>>>i : ", i, "<<<<<<<<<")
        rowD = dfDatas.iloc[i]

        # on extrait la cote (ou autre colonne précisée comme étant clé primaire)
        idRowD = rowD['shelfnumber']
        print("####################################")
        print("########### current DATA ###########")
        print("idrow", idRowD)         
        
        # on verifie l'existance d'une data avec cette cote par une requete
        # ------------
        # récupération/vérification d'une data par sa cote
        # Exemple de Search très précis où on précise 
        #   une cote dans un dcterms_identifier
        #   uniquement des datas (pas de collection)
        # rs = nklAPI_Search get_search_datas(nklT_prodEmpty, q="str_dcterms_identifier:UPOI_ATP_0001_0002", fq="scope=data", facet="", order="relevance")
        # ------------
        
        #rgetData = None
        
        # vérifier si on doit faire de la MAJ ou du Create
        if dataNklIdChecked=="":
            
            # on commence par un search
            rs = nklco.get_search_datas(nklTarget, q="str_dcterms_identifier:"+idRowD, fq="scope=data", facet="", order="relevance")
    
            print("Search : ",rs.dictVals)
                   
    
            if rs.isSuccess:
                # vérifier si la fonction find a trouvé 0 ou 1 ou N Datas avec cette cote
                # si O trouvé
                nbDataFinded = len(rs.dictVals['datas'])
                
                if nbDataFinded==0:
                    pass
                elif nbDataFinded==1:
                    #  la Data existe déjà sur Nakala selon Search
    
                    # on peut obtenir son DOI                    
                    dataNklId = rs.dictVals['datas'][0]['identifier']
                    print("Data finded by Search !",dataNklId )
                    
                    rgetData = nklco.get_datas(nklTarget, dataNklId, metadataFormat="qdc")
                    if rgetData.isSuccess:
                        
                        dataNklIdChecked = rgetData.dictVals['identifier']
                        
                        print(">>> Data Id checked : ", dataNklIdChecked)
                        needDataMaj = True
                        
                    else:
                        print("!! Data not finded by DOI....BUT find by Search !!!")
                        # c'est peut être une erreur reseau
                        print(rgetData)
                    
                else:
                    print("!! Multiple Data finded by Search !!!")
                    # on passe à la data suivante car cela à clairement un soucis potentiel
                    i+=1
            else:
                # erreur reseau
                print("prob com pr Search",rs)
                
                
        else:
        # on a un dataNklIdChecked issu d'un tour précédent 
        # donc on doit faire de la MAJ
            pass
        
               
        
           
        if needDataMaj==False:     
            #si needDataMAJ est a False c'est quon veut faire du create
            
            # si il n'existe pas cette Data il faut la créer mais avant
            # il faut pousser les Files qui lui sont associés (au moins 1)
            
            print("Data not found : Create requiered")
            # il faut pousser les Files qui lui sont associés (au moins 1)
        
            # isoler les lignes de dfFiles correspondant à la Data actuellement traité
            # en utilisant la colonne data_identifier
            # trié par order
            dfF = dfFiles[dfFiles['data_identifier']==idRowD]
            dfF= dfF.sort_values(by='order', ascending=True)
            print(dfF.head(n=5))
            
            # Pour chaque File on vérifie si il n'est pas déjà dans l'espace temporaire nakala uploads
            for idy, rowF in dfF.iterrows():
                if needDataMaj==False:
                    filename = rowF['file']
                    print("----------------------------------")
                    print("--------- current FILE -----------")
                    print(idy, filename) 
                    sha1=""
                    
                    # récupération/vérification des files ou leurs sha1 déposé non encore associés à une data
                    rup = nklco.get_datas_uploads(nklTarget)
                    #print(rup)
                    
                    b,sha1 = nklco.isFileNameInUploads(rup, filename)
                    #print(b,sha1)
                    
                    # si pas dans cet espace upload on l'envoi
                    if b==False:
                        pathFile = rowF["path"]+rowF["file"]
                        rPF = nklco.post_datas_uploads(nklTarget, pathFile)
                        
                        print(rPF)
                        # on stock son sha1 renvoyé par nakala 
                        # en cas d'échec sur ce fichier on passe au fichier suivant
                        if rPF.isSuccess:
                            sha1 = rPF.dictVals['sha1']
                            #print("fresh sha1",sha1 )
                            
                        else:
                            pass
                        
                    
                    # si déjà dans l'espace upload on a recup. son sha1
                    else:
                        print("FILE in uploads area with sha1", sha1)
                        
                     
                    

                    #print("sha1>>",sha1)  
                    # on peut creer un objet fileInfos
                    # un dico de clés : sha1 , description, embargoed
                    fileInfos = {}
                    fileInfos['sha1'] = sha1
                    fileInfos['description'] = rowF['description']
                    if str(rowF['embargoed'])=="nan":
                        # embargoed vide donc pas d'embargo 
                        # donc le server ajoutera par defaut la date du jour
                        pass
                    else:
                        # doit avoir une date
                        fileInfos['embargoed']=rowF['embargoed']

                    
                    #print(fileInfos)
                    
                    # on peut pousser notre Data avec son premier Fichier/fileInfos
                    # pour cela on doit convertir la ligne de meta row data en
                    # dicoNklData compatible pour conversion JSON nakala
                    # en utilisant confcol
                    dicoNklData = dfDataRow2nklDic(rowD, dfConfCol)
                    dicoNklData['files']=[fileInfos]

                    #print(dicoNklData)
                    
                    # envoyer la Data grace au dicoNklData
                    rPD = nklco.post_datas(nklTarget, dicoNklData)
                    print(rPD)
                    
                    if rPD.isSuccess:
                        dataNklIdChecked = rPD.dictVals['payload']['id']
                        needDataMaj=True
                        print("le doi de la data FRESH est", dataNklIdChecked)

                        
                    else:
                        pass
                
                
                else:
                    pass
                # sortir pour revenir 
                # à la vérification l'existance d'une data avec cette cote par une requete
                # normalement ça doit dire oui
                # donc on passera dans la section mise à jour
                # d'une data existante avec poursuite d'envoi des fichiers manquants
                
                    
            # on a parcouru tous les files sans jamais avoir reussi à en 
            # poser un a associer à une data dont la creation à fonctionnée
            if dataNklIdChecked=="":
                print("####### parcouru tt les files sans reussir a creer ?! #####")
                i+=1
                dataNklIdChecked=""
                needDataMaj=False

            
        else:
            print(">> MAJ requiered")
            #  la Data existe déjà sur Nakala 
            # son nklId est bien checked
            rgetData = nklco.get_datas(nklTarget, dataNklIdChecked, metadataFormat="qdc")
            
                
            # Pour chaque File
            
            # il faut pousser les Files manquants qui lui sont associés
        
            # isoler les lignes de dfFiles correspondant à la Data actuellement traité
            # en utilisant la colonne data_identifier
            # trié par order
            dfF = dfFiles[dfFiles['data_identifier']==idRowD]
            dfF= dfF.sort_values(by='order', ascending=True)
            print(dfF.head(n=5))
            
            
            # Pour chaque File on vérifie si il n'est pas déjà dans l'espace temporaire nakala uploads ni dans la data
            for idy, rowF in dfF.iterrows():
                filename = rowF['file']
                print("----------------------------------")
                print("--------- current FILE -----------")
                print(idy, filename) 
                # vérifier sa présence dans la Data distante
                bd,sd = nklco.isFileNameInData(rgetData, filename)
                
                print("file in data",bd,sd)
                
               
                # Si pas présent 
                if bd==False:
                    # on vérifie si il n'est pas non plus dans l'espace temporaire nakala uploads
                
                    rup = nklco.get_datas_uploads(nklTarget)                    
                    bu,su = nklco.isFileNameInUploads(rup, filename)
                    if bu==False:
                        print("file is not in temp nkl")
                    
                        # si pas présent non plus dans l'espace temporaire upload
                        
                        # on l'envoi
                        pathFile = rowF["path"]+rowF["file"]
                        rPF = nklco.post_datas_uploads(nklTarget, pathFile)
                        
                        print(rPF)
                        # on stock son sha1 renvoyé par nakala 
                        # en cas d'échec sur ce fichier on passe au fichier suivant
                        if rPF.isSuccess:
                            sha1 = rPF.dictVals['sha1']
                            print("fresh sha1",sha1 )                            
                        
                            # si envoi du fichier réussi on a son sha1
                            # on peut creer un objet fileInfos
                            # un dico de clés : sha1 , description, embargoed
                            # on l'ajout à la liste des objet fileInfos de la Data actuelle
                            # on envoi la requete de mise à jour des Files de la Data actuelle
    
                            fileInfos = {}
                            fileInfos['sha1'] = sha1
                            fileInfos['description'] = rowF['description']
                            if str(rowF['embargoed'])=="nan":
                                # embargoed vide donc pas d'embargo 
                                # donc le server ajoutera par defaut la date du jour
                                pass
                            else:
                                # doit avoir une date
                                fileInfos['embargoed']=rowF['embargoed']
                            # on ajoute ce fichier à la data
                            raddf = nklco.post_datas_files(nklTarget, dataNklIdChecked, fileInfos)
                            print(raddf)
                            
                            # on a donc son sha1 
                            # on complete dynamiquement la dataframe
                            dfF.at[idy, 'sha1'] = sha1
                            
                        else:
                            # echec de l'envoi du File ds l'espace temporaire
                            pass
                        
                        
                    else:
                        print("file in temp nkl:",bu,su)
                        
                        # on peut creer un objet fileInfos
                        # un dico de clés : sha1 , description, embargoed
                        fileInfos = {}
                        fileInfos['sha1'] = su
                        fileInfos['description'] = rowF['description']
                        if str(rowF['embargoed'])=="nan":
                            # embargoed vide donc pas d'embargo 
                            # donc le server ajoutera par defaut la date du jour
                            pass
                        else:
                            # doit avoir une date
                            fileInfos['embargoed']=rowF['embargoed']
                        # on ajoute ce fichier à la data  
                        raddf = nklco.post_datas_files(nklTarget, dataNklId, fileInfos)
                        print(raddf)
                        
                        #on a donc son sha1 
                        #on complete dynamiquement la dataframe
                        dfF.at[idy, 'sha1'] = su
                     
                        # si echec de l'envoi de ce fichier
                        # on passe au suivant                           
                    
    
                else:
                    # File déjà présent dans la Data
                    
                    #on a donc son sha1 
                    #on complete dynamiquement la dataframe
                    dfF.at[idy, 'sha1'] = sd
                    
            # on  peut mettre à jour les metadonnées
            # on peut aussi mettre à jour les Rights
            dicoNklData = dfDataRow2nklDic(rowD, dfConfCol)
            print("dicoNklData",dicoNklData)
            
                      
            rmm = nklco.put_datas(nklTarget, dataNklIdChecked, dicoNklData)
            print(rmm)
            if rmm.isSuccess==False:
                print(">>>> dicoNklData sended", dicoNklData)
            
            # au cours de ces traitement on a aussi été capable de récupérer le sha1 des fichiers de la data.
            # on peut donc appliquer une MAJ de l'ordonnancement, des descriptions et de l'embargo
            dicoNklFile = dfFileRows2nklDic(dfF)
            #print("dicFiles >>>>> ", dicoNklFile)
            rmmf = nklco.put_datas(nklTarget, dataNklIdChecked, dicoNklFile)
            print(rmmf)    
            if rmmf.isSuccess==False:
                print(">>>> dicoNklFile sended", dicoNklFile)
            
            
            # on a parcouru tous les File de la Data, 
            # donc on peut passer au traitement de la Data suivante
            
            i+=1
            dataNklIdChecked=""
            needDataMaj=False
            

                        

        
        
         
            

            
            
def dfDataRow2nklDic(rowD, dfConfCol, sepMultiVal="|"):
    """
    Permet de convertir une ligne de data
    vers un dictionnaire python transformable en json 
    pour une communication avec l'api nakala'
        
    
    
    Parameters
    rowD : une ligne de Dataframe Datas
    	une instance d'un objet dataframe pandas contenant des colonnes spécifiques DATA
        
    
        
    dfConfCol : Dataframe
    	une instance d'un objet dataframe pandas contenant des informations permettant
        de préciser la lang, le typeUri, le propertyUri de chaque colonne de dfData
        
    Returns
    -------
    List(Dict) : List(Dict)
        une liste contenant des dictionnaires python transformable en json
        pour une communication avec l'api nakala

    """
    
   
    
    dicData = {}
    dicData["collectionsIds"] = []

    # gestion du status
    #dicData["status"] = "pending"
    dicData["status"] = rowD['nklStatus']
    
    
    # gestion des droits (si precisé) dans le tableur dans la colonne nklRights 
    if 'nklRights' in rowD:
        print("rights specified <<<")
        listRight = rightsValuesToDic(rowD['nklRights'])
        
        if listRight == []:
            pass
        else:
            #si il a bien du contenu dans la ligne de cette colonne
            dicData['rights'] = listRight
    
    # gestion des collections (si precisé) dans le tableur dans la colonne nklCollections 
    if 'nklCollections' in rowD:
        print("nklCollections specified <<<")
        listCollections = collectionsValuesToDic(rowD['nklCollections'])
        
        if listCollections == []:
            pass
        else:
            #si il a bien du contenu dans la ligne de cette colonne
            dicData['collectionsIds'] = listCollections

    # -- gestion de partie data

    dicData["metas"] = []
    # chaque meta est composé de 4 clés value, lang, typeUri, propertyUri (sauf creator et status)
    
    # chaque colonne de dfData est complété par les valeurs de dfConfCol
    # print(rowD.keys())

    #parcourir chaque colonne de dfDataRow et vérifier
    # si dfConfCol contient des informations permettant une transformation
    # compatile nakala
    
    keyRowsD = rowD.keys()
    
    
    for colD in keyRowsD:
        
        dfRowFinded = dfConfCol[dfConfCol['DataColumnName']==colD]

        if len(dfRowFinded==1):
            
            
            # cas spécial pour les creators.
                       
            if not(colD == "creator") and not(colD == "nklStatus"):
            
                #--- value ----
                # gestion du multiValue
                
                listValues = rowD[colD].split(sepMultiVal)
                
                for currentValue in listValues:
                    dicMeta={}
                    
                    currentValue = currentValue.strip()
                    dicMeta['value'] = currentValue
                    
                    #--- lang -----
                    lang = ""
                    if not(str(dfRowFinded.iloc[0]['Nkl-lang'])=="nan"):
                    
                        lang = str(dfRowFinded.iloc[0]['Nkl-lang'])
                        dicMeta['lang'] = lang
                    
    
                    #--- typeUri -----
                    typeUri = ""
                    if not(dfRowFinded.iloc[0]['Nkl-typeUri']==""):
                    
                        typeUri = str(dfRowFinded.iloc[0]['Nkl-typeUri'])
                    
                    dicMeta['typeUri'] = typeUri                    
                    
                    #--- propertyUri -----
                    propertyUri = ""
                    if not(dfRowFinded.iloc[0]['Nkl-propertyUri']==""):
                    
                        propertyUri = str(dfRowFinded.iloc[0]['Nkl-propertyUri'])
                    
                    dicMeta['propertyUri'] = propertyUri 
    
                    dicData["metas"].append(dicMeta)
                
            else:
                strCreators = rowD['creator']
                listDicCreators = creatorsValuesToDic(strCreators,sepSurname=",",sepOrcid="@",sepCreator="|")
                for dicCreator in listDicCreators:
                    dicData["metas"].append(dicCreator)
             
        
        elif len(dfRowFinded>1):
            print("warning : plusieurs conf. pour une même colonne",colD)
        else:
            # colonne non configuré mais c'est normal
           # normal uniquement si il ne s'agit pas d'une méta à décrire par un quadruplet"
            if colD in ["nklStatus","nklIdentifier"]:
                pass
            else:
                print("warning colonne non configuré", colD)
            
        

    
    return dicData            
        
                        
        
def creatorsValuesToDic(strCreators,sepSurname=",",sepOrcid="@",sepCreator="|"):
    """
    Permet de convertir la chaine de caractère contenant
    un ou plusieurs creators vers une liste de dico respectant
    la structure json nakala attendu

    Parameters
    strCreators : STR
        une string formaté en prénom, nom @orcid séparé par des |
    	
        
    Returns
    -------
    List(Dict) : List(Dict)
        une liste contenant des dictionnaires python transformable en json
        pour une communication avec l'api nakala

    """
    
    listResult = []
    # séparer les individu grace au séparateur | sepCreator
    #listCreator = strCreators.split("|")
    listCreator = strCreators.split(sepCreator)

    
    for creator in listCreator:
        
        target = r" *(?P<surname>[\w\- ]*) *, *(?P<givenname>[\w\-\. ]*) *(@(?P<orcid>[\w\-]*))* *"
        match = re.search(target, creator, re.IGNORECASE)
        if match:
            #print("givename",match.group('givename'))
            #print("surname",match.group('surname'))
            #print("orcid",match.group('orcid'))


            metaAuthor = {}
            metaAuthor["value"]={}
            metaAuthor["value"]["givenname"]=""
            metaAuthor["value"]["surname"]=""
            metaAuthor["value"]["orcid"]=""
            metaAuthor['propertyUri']="http://nakala.fr/terms#creator"

            if not (match.group('givenname') == None):
               metaAuthor["value"]["givenname"]=match.group('givenname')
               
            if not (match.group('surname') == None):
               metaAuthor["value"]["surname"]=match.group('surname')            
            
            if not (match.group('orcid') == None):
               metaAuthor["value"]["orcid"]=match.group('orcid') 
               
               
            listResult.append(metaAuthor)
        else:
            print("not found")
        
    
    return listResult
        
    
    
def dfFileRows2nklDic(dfFileRows):
    """
    Permet de convertir les lignes de file d'une data
    vers un dictionnaire python transformable en json 
    pour une communication avec l'api nakala'
        
    
    
    Parameters
    dfFileRows : des lignes de Dataframe Files
    	une instance d'un objet dataframe pandas contenant des colonnes spécifiques FILE
        
            
    Returns
    -------
    Dict : Dict
        dictionnaire python transformable en json
        pour une communication avec l'api nakala

    """
       
    
    dicFiles = {"files":[]}
        
    for idy, rowF in dfFileRows.iterrows():
        dicF = {}
        dicF["name"] = rowF['file']
        dicF["sha1"] = rowF["sha1"]
        dicF["description"] = rowF["description"]   
        
        if str(rowF['embargoed'])=="nan":
            # embargoed vide donc pas d'embargo 
            # donc le server ajoutera par defaut la date du jour
            pass
        else:
            # doit avoir une date
            dicF['embargoed']=rowF['embargoed']
        
        dicFiles["files"].append(dicF)
        
    return dicFiles            



        
def rightsValuesToDic(strRights,sepRole=",",sepUserGroup="|"):
    """
    Permet de convertir la chaine de caractère contenant
    un ou plusieurs userGroup vers une liste de dico respectant
    la structure json nakala attendu

    Parameters
    strRights : STR
        une string formaté en id, role séparé par des |
    	
        
    Returns
    -------
    List(Dict) : List(Dict)
        une liste contenant des dictionnaires python transformable en json
        pour une communication avec l'api nakala

    """
    
    listResult = []
    # séparer les userGroup grace au séparateur | sepUserGroup
    listUserGroup = strRights.split(sepUserGroup)
    
    for userGroup in listUserGroup:
        
        target = r" *(?P<idUserGroup>[\w\- ]*) *, *(?P<role>[\w]+) *"
        match = re.search(target, userGroup, re.IGNORECASE)
        if match:
            #print("idUserGroup",match.group('idUserGroup'))
            #print("role",match.group('role'))

           
            metaRights={}
            metaRights["id"] = match.group('idUserGroup')
            metaRights["role"] = match.group('role')

            listResult.append(metaRights)
            
        else:
            print("Rights values : not found or bad formatting")
        
    
    return listResult


def collectionsValuesToDic(strCollections, sepCollection="|"):
    """
    Permet de convertir la chaine de caractère contenant
    une ou plusieurs collections vers une liste respectant
    la structure json nakala attendu

    Parameters
    strCollections : STR
        une string formaté en identifiant (DOI like) de collection séparé par des |
    	
        
    Returns
    -------
    List(STR) : List(STR)
        une liste contenant des string python transformable en json
        pour une communication avec l'api nakala

    """
    
    listResult = []
    # séparer les userGroup grace au séparateur | sepUserGroup
    listUserGroup = strCollections.split(sepCollection)
    
    print("strCollections", strCollections)
    
    for userGroup in listUserGroup:
        
        target = r" *(?P<collectionId>[\w\d\.\/]+)"
        match = re.search(target, userGroup, re.IGNORECASE)
        if match:
           
            print("match", match)

            listResult.append(match.group('collectionId'))


            
        else:
            print("collection values : not found or bad formatting")
        
    
    return listResult    
        
    
    

    

    
    
    